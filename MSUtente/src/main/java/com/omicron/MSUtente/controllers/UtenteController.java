package com.omicron.MSUtente.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/utente")
public class UtenteController {

	@Autowired
	private Environment env;
	
	@GetMapping("/status")
	public String status() {
		return "Ciao, sono avviato sulla porta: " + env.getProperty("local.server.port");
	}
	
}
